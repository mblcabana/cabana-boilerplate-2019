﻿using ImageProcessor;
using ImageProcessor.Imaging.Formats;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Configuration;
using Umbraco.Core.Configuration.UmbracoSettings;
using Umbraco.Core.IO;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace CabanaBoilerTemplate
{

    // GET: ImageQuality
    public class ImageProcessorEventHandler : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            // Tap into the Saving event
            MediaService.Saving += (sender, args) =>
            {
                MediaFileSystem mediaFileSystem = FileSystemProviderManager.Current.GetFileSystemProvider<MediaFileSystem>();
                IContentSection contentSection = UmbracoConfig.For.UmbracoSettings().Content;
                IEnumerable<string> supportedTypes = contentSection.ImageFileTypes.ToList();
                ISupportedImageFormat format = new JpegFormat { Quality = 70 };
                foreach (IMedia media in args.SavedEntities)
                {
                    if (media.HasProperty("umbracoFile"))
                    {
                        // Make sure it's an image.
                        string path = media.GetValue<string>("umbracoFile");
                        string extension = Path.GetExtension(path).Substring(1);
                        if (supportedTypes.InvariantContains(extension))
                        {
                            // Resize the image to 1920px wide, height is driven by the
                            // aspect ratio of the image.
                            string fullPath = mediaFileSystem.GetFullPath(path);
                            using (ImageFactory imageFactory = new ImageFactory(true))
                            {
                                //ResizeLayer layer = new ResizeLayer(new Size(2120, 0), ResizeMode.Max)
                                //{
                                //    Upscale = false
                                //};

                                imageFactory.Load(fullPath)
                                            //.Resize(layer)
                                            .Format(format)
                                            .Save(fullPath);
                            }
                        }
                    }
                }
            };
        }
    }
}